module gitlab.com/de1ux/realtime-canvas-editor/server

go 1.12

require (
	github.com/golang/protobuf v1.3.1
	github.com/gorilla/websocket v1.4.0 // indirect
	github.com/improbable-eng/grpc-web v0.9.1
	github.com/rs/cors v1.6.0 // indirect
	google.golang.org/grpc v1.20.1
)
