package main

import (
	"context"
	"github.com/improbable-eng/grpc-web/go/grpcweb"
	"gitlab.com/de1ux/realtime-canvas-editor/server/generated/api"
	"google.golang.org/grpc"
	"log"
	"net/http"
)

var ()

func init() {

}

func NewService() *service {
	s :=  &service{
		in:  make(chan *api.Drawing, 100),
		out: make(chan *api.Drawing, 100),
	}
	return s.Start()
}

type service struct {
	drawings []*api.Drawing
	streams  []api.DrawService_GetDrawingsServer
	in       chan *api.Drawing
	out      chan *api.Drawing
}

func (s *service) Start() *service {
	go func() {
		log.Print("Listening in...")
		for d := range s.in {
			s.drawings = append(s.drawings, d)
			s.out <- d
		}
		log.Print("Listening in...done")
	}()

	go func() {
		log.Print("Listening out...")
		for d := range s.out {
			for _, stream := range s.streams {
				if err := stream.Send(&api.Drawings{Drawings: []*api.Drawing{d}}); err != nil {
					log.Printf("Failed to send on stream (already disconnected?): %s", err)
				}
			}
		}
		log.Print("Listening out...done")
	}()
	return s
}

func (s *service) SendDrawing(_ context.Context, d *api.Drawing) (*api.None, error) {
	s.in <- d
	return &api.None{}, nil
}

func (s *service) GetDrawings(_ *api.None, stream api.DrawService_GetDrawingsServer) error {
	s.streams = append(s.streams, stream) // TODO - this is not safe
	select {}
}

func main() {
	grpcServer := grpc.NewServer()

	s := NewService()
	api.RegisterDrawServiceServer(grpcServer, s)

	wrappedGrpcServer := grpcweb.WrapServer(grpcServer)

	log.Print("Accepting requests...")
	if err := (&http.Server{
		Handler: http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set("Access-Control-Allow-Origin", "*")
			w.Header().Set("Access-Control-Allow-Headers", "*")
			w.Header().Set("Access-Control-Allow-Headers", "*")

			wrappedGrpcServer.ServeHTTP(w, r)
		}),

		Addr: "0.0.0.0:3001",
	}).ListenAndServe(); err != nil {
		log.Fatal(err)
	}
}
