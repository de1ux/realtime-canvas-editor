import {DrawServiceClient} from "./generated/api/api_pb_service";
import {Drawing, Drawings, None} from "./generated/api/api_pb";

let canvas = document.getElementById("main") as HTMLCanvasElement;
canvas.setAttribute("width", String(window.innerWidth));
canvas.setAttribute("height", String(window.innerHeight));

let ctx = canvas.getContext("2d");
ctx.fillStyle = "black";

let client = new DrawServiceClient("http://localhost:3001");
let drawingSubscription  = client.getDrawings(new None());

drawingSubscription.on("data", (ds: Drawings) => {
    let d = ds.getDrawingsList()[0];
    ctx.fillRect(d.getX(), d.getY(), 10, 10);
});

document.onmousemove = (e: MouseEvent) => {
    let d = new Drawing();
    d.setX(e.x);
    d.setY(e.y);
    ctx.fillRect(d.getX(), d.getY(), 10, 10);
    client.sendDrawing(d, () => {});
};