.PHONY: build api

build: clean api
	go build server/main.go

api: clean
	mkdir -p app/src/generated
	mkdir -p server/generated
	protoc \
	    --plugin="protoc-gen-ts=app/node_modules/ts-protoc-gen/bin/protoc-gen-ts" \
	    --ts_out="service=true:app/src/generated" \
	    --js_out="import_style=commonjs,binary:app/src/generated" \
	    --go_out="plugins=grpc:server/generated" \
	    api/api.proto

clean:
	rm -rf app/src/generated
	rm -rf server/generated

