import * as puppeteer from 'puppeteer';

function sleep (time) {
  return new Promise((resolve) => setTimeout(resolve, time));
}

(async () => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await page.goto('http://localhost:3000');

  console.log("Looping on move...");
  for (let i = 0; i < 1000; i++) {
    await page.mouse.move(Math.random() * 500, Math.random() * 500);
    await sleep(100);
  }
  console.log("Looping on move...done");

  await browser.close();
})();